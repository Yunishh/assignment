import "./App.css";
import RegistrationForm from "./components/Registration/RegistrationForm";

function App() {
  return (
    <div className="App">
      <RegistrationForm />
    </div>
  );
}

export default App;
