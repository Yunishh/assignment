export const GenderOptions = [
  { label: "Male", value: "Male" },
  { label: "Female", value: "Female" },
  { label: "Others", value: "Others" },
];

export const CountryList = [
  { value: "Nepal", label: "Nepal" },
  { value: "China", label: "China" },
  { value: "India", label: "India" },
  { value: "USA", label: "USA" },
  { value: "Canada", label: "Canada" },
  { value: "Brazil", label: "Brazil" },
  { value: "Argentina", label: "Argentina" },
  { value: "Bangladesh", label: "Bangladesh" },
  { value: "Afghanistan", label: "Afghanistan" },
  { value: "Sri Lanka", label: "Sri Lanka" },
  { value: "Ghana", label: "Ghana" },
  { value: "Egypt", label: "Egypt" },
  { value: "Spain", label: "Spain" },
];
