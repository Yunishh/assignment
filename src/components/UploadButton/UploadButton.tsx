import React from "react";
import { UploadOutlined } from "@ant-design/icons";
import { Typography } from "antd";

function UploadButton() {
  return (
    <div>
      <UploadOutlined />
      <Typography.Text type="secondary" style={{ marginLeft: 8 }}>
        Upload
      </Typography.Text>
    </div>
  );
}

export default UploadButton;
