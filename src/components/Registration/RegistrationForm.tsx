import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  DatePicker,
  Radio,
  Row,
  Select,
  Space,
  Typography,
  Upload,
} from "antd";
import type { RcFile } from "antd/es/upload";
import type { UploadFile } from "antd/es/upload/interface";
import {
  ArrayHelpers,
  ErrorMessage,
  Field,
  FieldArray,
  Form,
  Formik,
} from "formik";
import React, { useState } from "react";
import { CountryList, GenderOptions } from "../../constants/constants";
import { getBase64 } from "../../utils/getBase64";
import InputField from "../FormComponents/InputField";
import UploadButton from "../UploadButton/UploadButton";
import ViewImageModal from "../ViewImageModal/ViewImageModal";
import { IRegistrationForm } from "./registrationForm.interface";
import {
  registrationFormInitialValues,
  registrationFormValidationSchema,
} from "./registrationForm.schema";
function RegistrationForm() {
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");

  const handleCancel = () => setPreviewOpen(false);

  const handlePreview = async (file: UploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj as RcFile);
    }

    setPreviewImage(file.url || (file.preview as string));
    setPreviewOpen(true);
    setPreviewTitle(
      file.name || file.url!.substring(file.url!.lastIndexOf("/") + 1)
    );
  };

  return (
    <Formik
      initialValues={registrationFormInitialValues}
      validationSchema={registrationFormValidationSchema}
      enableReinitialize
      onSubmit={(values: IRegistrationForm) => {
        console.log(values, "values");
        alert(JSON.stringify(values));
      }}
    >
      {(formik) => {
        const { values, setFieldValue, errors } = formik;
        return (
          <Form style={{ padding: "1rem" }}>
            <Card style={{ width: "100%", margin: "0.5rem" }}>
              <Typography.Title
                type="success"
                level={5}
                style={{ marginTop: "0" }}
              >
                Personal Details
              </Typography.Title>
              <Row align="middle" justify={"center"}>
                <Col span={4}>
                  <Field name="profileImage" style={{ width: "100%" }}>
                    {({
                      field,
                    }: {
                      field: {
                        name: string;
                        value: UploadFile;
                        onChange: (e: UploadFile) => void;
                      };
                    }) => (
                      <Upload
                        {...field}
                        style={{ width: "100%" }}
                        name="profileImage"
                        beforeUpload={() => false}
                        listType="picture-card"
                        fileList={values.profileImage}
                        onPreview={handlePreview}
                        onChange={({ fileList: newFileList }) =>
                          setFieldValue("profileImage", newFileList)
                        }
                      >
                        {values.profileImage.length >= 1 ? null : (
                          <UploadButton />
                        )}
                      </Upload>
                    )}
                  </Field>
                  <div>
                    <Typography.Text
                      type="secondary"
                      style={{ marginBottom: "10px" }}
                    >
                      Profile Picture
                    </Typography.Text>
                  </div>

                  <ErrorMessage
                    name={"profileImage"}
                    render={(errMsg: string) => (
                      <Typography.Text italic type="danger">
                        {errMsg}
                      </Typography.Text>
                    )}
                  />
                  <ViewImageModal
                    previewOpen={previewOpen}
                    previewTitle={previewTitle}
                    handleCancel={handleCancel}
                    previewImage={previewImage}
                  />
                </Col>
                <Col span={20}>
                  <Row gutter={8}>
                    <Col span={8}>
                      <InputField
                        name="firstName"
                        label="First Name"
                        {...formik}
                      />
                    </Col>
                    <Col span={8}>
                      <InputField
                        name="middleName"
                        label="Middle Name"
                        {...formik}
                      />
                    </Col>
                    <Col span={8}>
                      <InputField
                        name="lastName"
                        label="Last Name"
                        {...formik}
                      />
                    </Col>
                    <Col span={8}>
                      <Space.Compact block>
                        <div style={{ marginBottom: "10px", width: "100%" }}>
                          <Typography.Text type="secondary">
                            Date of birth
                          </Typography.Text>
                          <DatePicker
                            style={{ width: "100%" }}
                            name={"dateOfBirth"}
                            status={errors.dateOfBirth ? "error" : ""}
                            onChange={(date, dateString) =>
                              setFieldValue("dateOfBirth", dateString)
                            }
                            size={"middle"}
                          />
                          <ErrorMessage
                            name={"dateOfBirth"}
                            render={(errMsg: string) => (
                              <Typography.Text italic type="danger">
                                {errMsg}
                              </Typography.Text>
                            )}
                          />
                        </div>
                      </Space.Compact>
                    </Col>
                    <Col span={8}>
                      <InputField name={`email`} label="Email" {...formik} />
                    </Col>
                    <Col span={8}>
                      <InputField
                        name={`contactNumber`}
                        label="Contact Number"
                        {...formik}
                      />
                    </Col>
                    <Col span={8}>
                      <Typography.Text type="secondary">Gender</Typography.Text>
                      <Radio.Group
                        style={{ width: "100%" }}
                        options={GenderOptions}
                        onChange={(event) => {
                          const { value } = event.target;
                          setFieldValue("gender", value);
                        }}
                        value={values.gender}
                      />
                      <ErrorMessage
                        name={"gender"}
                        render={(errMsg: string) => (
                          <Typography.Text italic type="danger">
                            {errMsg}
                          </Typography.Text>
                        )}
                      />
                    </Col>
                    <Col span={8}>
                      <Typography.Text type="secondary">
                        Country
                      </Typography.Text>
                      <Select
                        style={{ width: "100%" }}
                        status={errors.country ? "error" : ""}
                        onChange={(value) => setFieldValue("country", value)}
                        options={CountryList}
                      />
                      <ErrorMessage
                        name={"country"}
                        render={(errMsg: string) => (
                          <Typography.Text italic type="danger">
                            {errMsg}
                          </Typography.Text>
                        )}
                      />
                    </Col>
                    <Col span={8}>
                      <InputField name="address" label="Address" {...formik} />
                    </Col>
                    <Col span={8}>
                      <InputField
                        name="fatherFirstName"
                        label="Father's First Name"
                        {...formik}
                      />
                    </Col>
                    <Col span={8}>
                      <InputField
                        name="fatherMiddleName"
                        label="Father's Middle Name"
                        {...formik}
                      />
                    </Col>
                    <Col span={8}>
                      <InputField
                        name="fatherLastName"
                        label="Father's Last Name"
                        {...formik}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Card>
            {/* use field Array for education details */}

            <Card style={{ width: "100%", margin: "0.5rem" }}>
              <Typography.Title
                type="success"
                level={5}
                style={{ marginTop: "0" }}
              >
                Academic Details
              </Typography.Title>
              <FieldArray
                name="educationDetails"
                render={(arrayHelpers: ArrayHelpers) => (
                  <>
                    {values.educationDetails &&
                    values.educationDetails.length > 0 ? (
                      values.educationDetails.map((education, index) => (
                        <React.Fragment key={index}>
                          <Row
                            gutter={8}
                            style={{
                              alignItems: "center",
                            }}
                          >
                            <Col span={6}>
                              <InputField
                                name={`educationDetails.${index}.degree`}
                                label="Degree"
                                {...formik}
                              />
                            </Col>
                            <Col span={6}>
                              <InputField
                                name={`educationDetails.${index}.percentage`}
                                label="Percentage"
                                {...formik}
                              />
                            </Col>
                            <Col span={6}>
                              <InputField
                                name={`educationDetails.${index}.yearOfCompletion`}
                                label="Year Of Completion"
                                type="number"
                                {...formik}
                              />
                            </Col>
                            <Col span={6}>
                              <Button
                                icon={<PlusOutlined />}
                                htmlType="button"
                                style={{ marginRight: "0.5rem" }}
                                onClick={() =>
                                  arrayHelpers.push({
                                    degree: "",
                                    percentage: "",
                                  })
                                }
                              />
                              <Button
                                icon={<DeleteOutlined />}
                                htmlType="button"
                                onClick={() => arrayHelpers.remove(index)}
                              />
                            </Col>
                          </Row>
                        </React.Fragment>
                      ))
                    ) : (
                      <Button
                        icon={<PlusOutlined />}
                        htmlType="button"
                        onClick={() =>
                          arrayHelpers.push({
                            degree: "",
                            percentage: "",
                          })
                        }
                      >
                        Add Academic Details
                      </Button>
                    )}
                  </>
                )}
              />
            </Card>
            <Row justify={"end"}>
              <Button size="large" type="primary" htmlType="submit">
                Submit
              </Button>
            </Row>
          </Form>
        );
      }}
    </Formik>
  );
}

export default RegistrationForm;
