import type { UploadFile } from "antd/es/upload/interface";

export interface IRegistrationForm {
  firstName: string;
  middleName: string;
  lastName: string;
  fatherFirstName: string;
  fatherMiddleName: string;
  fatherLastName: string;
  email: string;
  contactNumber: string;
  educationDetails: {
    degree: string;
    percentage: string;
    yearOfCompletion: number;
  }[];
  profileImage: UploadFile[];
  dateOfBirth: string;
  gender: string;
  address: string;
  country: string;
}
