import * as Yup from "yup";
export const registrationFormInitialValues = {
  firstName: "",
  middleName: "",
  lastName: "",
  fatherFirstName: "",
  fatherMiddleName: "",
  fatherLastName: "",
  contactNumber: "",
  email: "Male",
  dateOfBirth: "",
  gender: "",
  address: "",
  country: "",
  educationDetails: [{ degree: "", percentage: "", yearOfCompletion: 0 }],
  profileImage: [],
};

export const registrationFormValidationSchema = Yup.object({
  firstName: Yup.string()
    .min(3, "First name must be minimum of 3 characters")
    .max(20, "First name must be maximum of 20 characters")
    .required("First name is required!"),
  lastName: Yup.string()
    .min(3, "Last name must be minimum of 3 characters")
    .max(20, "Last name must be maximum of 20 characters")
    .required("Last name is required!"),
  fatherFirstName: Yup.string()
    .min(3, "Father's first name must be minimum of 3 characters")
    .max(20, "Father's first name must be maximum of 20 characters")
    .required("Father's first name is required!"),
  fatherLastName: Yup.string()
    .min(3, "Father's last name must be minimum of 3 characters")
    .max(20, "Father's last name must be maximum of 20 characters")
    .required("Father's last name is required!"),
  contactNumber: Yup.string()
    .min(9, "Contact number must be minimum of 9 characters")
    .max(15, "Contact number must be maximum of 15 characters")
    .required("Contact number is required!"),
  dateOfBirth: Yup.string().required("Date of birth is required!"),
  email: Yup.string()
    .email("Email must be in email format")
    .required("Email is required!"),
  gender: Yup.string().required("Gender is required!"),
  address: Yup.string().required("Address is required!"),
  country: Yup.string().required("Country is required!"),
  profileImage: Yup.array()
    .required("Please select a file")
    .min(1, "You have to upload a file")
    .test("fileType", "Only JPG/PNG files are allowed", (value) => {
      if (!value || value.length === 0) {
        return true; // No file selected, so it's valid
      }
      return value[0].type === "image/jpeg" || value[0].type === "image/png";
    })
    .test("fileSize", "File size must be smaller than 2MB", (value) => {
      if (!value || value.length === 0) {
        return true; // No file selected, so it's valid
      }
      return value[0].size / 1024 / 1024 < 2;
    }),
  educationDetails: Yup.array()
    .of(
      Yup.object().shape({
        degree: Yup.string().required("Degree is required!"),
        percentage: Yup.string().required("Percentage is required!"),
        yearOfCompletion: Yup.number()
          .min(1990, "Year of completetion must be greater than 1990")
          .required("Year of completion is required!"),
      })
    )
    .required("Must have education Details!")
    .min(1, "Minimum of 1 education degree is required!"),
});
