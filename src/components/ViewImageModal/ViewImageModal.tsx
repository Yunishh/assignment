import { Modal } from "antd";
import React from "react";

interface IViewImageModal {
  previewOpen: boolean;
  previewTitle: string;
  handleCancel: () => void;
  previewImage: string;
}
function ViewImageModal({
  previewOpen,
  previewTitle,
  handleCancel,
  previewImage,
}: IViewImageModal) {
  return (
    <Modal
      open={previewOpen}
      title={previewTitle}
      footer={null}
      onCancel={handleCancel}
    >
      <img alt="example" style={{ width: "100%" }} src={previewImage} />
    </Modal>
  );
}

export default ViewImageModal;
