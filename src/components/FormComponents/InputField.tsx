import { Input, Space, Typography } from "antd";
import { ErrorMessage, FormikErrors } from "formik";
import { IRegistrationForm } from "../Registration/registrationForm.interface";

const { Text } = Typography;

interface IInputField {
  name: string;
  label: string;
  type?: string;
  setFieldValue: (
    field: string,
    value: string,
    shouldValidate?: boolean
  ) => Promise<void | FormikErrors<IRegistrationForm>>;
  errors: FormikErrors<IRegistrationForm>;
}

function InputField({
  name,
  label,
  errors,
  setFieldValue,
  type = "text",
}: IInputField) {
  return (
    <Space.Compact block>
      <div style={{ marginBottom: "10px", width: "100%" }}>
        <Text type="secondary">{label}</Text>
        <Input
          id={name}
          name={name}
          type={type}
          status={errors[name as keyof IRegistrationForm] ? "error" : ""}
          onChange={(event) => {
            const { name, value } = event.target;
            setFieldValue(name, value);
          }}
        />
        <ErrorMessage
          name={name}
          render={(errMsg: string) => (
            <Text italic type="danger">
              {errMsg}
            </Text>
          )}
        />
      </div>
    </Space.Compact>
  );
}

export default InputField;
